import configparser
from os import remove
from pathlib import Path

_CONFIG_DIR = Path.home() / ".splitAndSend"
_CONFIGFILE = _CONFIG_DIR / "splitAndSend.ini"

def create_dir():
    if not _CONFIG_DIR.is_dir():
        _CONFIG_DIR.mkdir()

def initialize():
    create_dir()
    config = configparser.ConfigParser()

    config["User"] = {"email":"c.n.copol@gmail.com"}

    config["Email_editor"] = {"subject": "Sujet",
                              "contents": "Bonjour $Nom $Prenom\n\nVeuillez trouver le QCM\n\nCordialement\nJudy DAMO"
    }


    config["Mailing"] = {}#"nom_1": "chemin_1.csv"}

    # with open(_CONFIGFILE, 'w') as configfile:
    #     config.write(configfile)

    # config = configparser.ConfigParser()
    # config['DEFAULT'] = {'ServerAliveInterval': '45',
    #                      'Compression': 'yes',
    #                      'CompressionLevel': '9'}
    # config['bitbucket.org'] = {}
    # config['bitbucket.org']['User'] = 'hg'
    # config['topsecret.server.com'] = {}
    # topsecret = config['topsecret.server.com']
    # topsecret['Port'] = '50022'     # mutates the parser
    # topsecret['ForwardX11'] = 'no'  # same here
    # config['DEFAULT']['ForwardX11'] = 'yes'
    with open(_CONFIGFILE, 'w') as configfile:
        config.write(configfile)

def read():
    config = configparser.ConfigParser()
    config.read(_CONFIGFILE)

    return config

def write(config):
    with open(_CONFIGFILE, 'w') as configfile:
        config.write(configfile)


def add_classe(config, name, filename):
    config["Mailing"][name] = filename


def get_mailing_list(config):
    """Returns a list of dict. Each dict gives the name of the mailing list and the path of the csv.
    """
    return { key: {'name':key, 'filepath':_CONFIG_DIR / value} for i, (key,value) in enumerate(config["Mailing"].items())}
    # return {key:_CONFIG_DIR / value for key, value in }


def check_files(config):
    removeMe = []
    for name, filepath in config["Mailing"].items():
        if not (_CONFIG_DIR / filepath).is_file():
            removeMe.append(name)

    for name in removeMe:
        config["Mailing"].pop(name)

    write(config)