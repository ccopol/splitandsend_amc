
from pathlib import Path
from string import Template
import shutil
import sys
import time

import keyring
from PySide6 import QtCore, QtGui
from PySide6.QtWidgets import (QApplication, QCheckBox, QDialog, QDialogButtonBox, QFileDialog, QGridLayout, QLabel, QListWidget, QListWidgetItem, QMainWindow, QMenu, QPushButton,
                               QTableWidgetItem, QPlainTextEdit, QTextEdit, QProgressBar)
from PySide6.QtWidgets import (QHBoxLayout, QVBoxLayout, QSplitter,
                               QWidget, QLabel, QSpinBox, QLineEdit,
                               QHeaderView, QGroupBox, QTableWidget,
                               QDialogButtonBox, QMessageBox)

from PyPDF2 import PdfFileReader, PdfFileWriter, pdf
import yagmail

# Local import
import configurator
import readers

# Position de Nom, Prenom, Email et Classe dans le tableau de mailing
NOM_COLUMN    = 0
PRENOM_COLUMN = 1
EMAIL_COLUMN  = 2
CLASSE_COLUMN = 3


class Worker(QtCore.QObject):
    finished = QtCore.Signal()
    progress = QtCore.Signal(int)
    logText  = QtCore.Signal(str)

    def init_email(self, myEmail, subject, contents_as_template, mailing_list):
        self.myEmail = myEmail
        self.subject = subject
        self.contents_as_template = contents_as_template
        self.mailing_list = mailing_list

    def init_attachment(self, pdffilepath, nbPagesByQCM):
        self.pdfname = pdffilepath
        self.nbPagesByQCM = nbPagesByQCM

    def run(self):
        self.progress.emit(1)

        self.logText.emit("Séparation des fichiers PDF")
        pdflists, pdftempdir = readers.PDFSplit(str(self.pdfname), self.nbPagesByQCM)

        students = self.mailing_list

        csvfile_for_AMC = self.pdfname.parent / (self.pdfname.stem + "_for_AMC_.csv")
        self.logText.emit("Envoie des emails")
        with yagmail.SMTP(self.myEmail) as yag, open(csvfile_for_AMC, "w") as csv_for_AMC:
            print("Nom,Prenom,Email,PDF", file=csv_for_AMC)
            nb_receivers = len(students)
            for i, (pdffile, receiver) in enumerate(zip(pdflists, students)):
                mapping = { var:receiver[idx] for idx, var in enumerate(["Nom", "Prenom", "Email", "Classe"])}
                newpdffile = pdffile.with_name(f"{self.pdfname.stem}_{mapping['Nom'].upper()}_{mapping['Prenom'].lower().capitalize()}.pdf")
                pdffile.rename(newpdffile)
                try:
                    try:
                        email_contents = self.contents_as_template.substitute(mapping)
                    except:
                        self.logText.emit("soucis avec email_contents template")
                    yag.send(mapping["Email"], self.subject, email_contents, attachments=newpdffile)
                    print(f"{i+1} {mapping['Nom']},{mapping['Prenom']},{mapping['Email']},{newpdffile.name}",file=csv_for_AMC)

                except KeyError as valerr:
                    self.logText.emit(f"Aucune colonne ne se nomme ainsi: {valerr}. Les valeurs acceptables sont:\n\t{','.join(self.csv_header)}")
                    shutil.rmtree(pdftempdir)
                self.logText.emit(f"Envoie {i+1:3}/{nb_receivers}: {','.join(receiver)} {newpdffile.name}")
                self.progress.emit((i+1)/nb_receivers*100)

        shutil.rmtree(pdftempdir)

        self.logText.emit("Envoie terminée")

        self.logText.emit(f"CSV sauvegardé: {csvfile_for_AMC}")
        self.finished.emit()


class QLogin(QDialog):
    isValid = QtCore.Signal(bool)

    def __init__(self):
        super().__init__()

        self.setWindowTitle("Paramètre de connexion")

        self._layout = QVBoxLayout()
        self.createUI()

    def createUI(self):
        self.setMinimumSize(500, 300)
        hbox = QHBoxLayout()
        hbox.addWidget(QLabel("Email:"))
        self.emailWidget = QLineEdit()
        self.emailWidget.setPlaceholderText("Entrez votre mail")
        hbox.addWidget(self.emailWidget)
        hbox.stretch(2)
        self._layout.addLayout(hbox)
        

        hbox = QHBoxLayout()
        hbox.addWidget(QLabel("Password:"))
        self.passwordWidget = QLineEdit()
        self.passwordWidget.setPlaceholderText("Password")
        self.passwordWidget.setEchoMode(QLineEdit.Password)
        hbox.addWidget(self.passwordWidget)
        self._layout.addLayout(hbox)
        #   String regex = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
        self.rememberMe = QCheckBox()

        # self.layout.addWidget(self.rememberMe)

        # QBtn = QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel
        QBtn = QDialogButtonBox.StandardButton.Ok
        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        # self.buttonBox.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        # message = QLabel("Something happened, is that OK?")
        # self._layout.addWidget(message)
        self._layout.addWidget(self.buttonBox)
        # self.setLayout(self.layout)

        self.setLayout(self._layout)

    def signin(self):
        self.show()

    def getEmail(self):
        return self.emailWidget.text()

    def accept(self):

        passwd = keyring.get_password("yagmail", self.emailWidget.text())
        if passwd is None:
            yagmail.register(self.emailWidget.text(), self.passwordWidget.text())
        else:
            print("Mot de passe déjà utilise")
            if passwd == self.passwordWidget.text():
                self.update_config()
                # self.close()
                # return None

        try:
            with yagmail.SMTP(self.emailWidget.text()) as yag:
                yag.send(subject="test reussi")
        except:
            print("Envoie non reussi")
            self.isValid.emit(False)

            ret = QMessageBox.critical(self, "Email de test non envoyé",
                                       "Le mail n’a pas pu être envoyé.\n"
                                       "Veuillez vérifier vos identifiants",
                                       QMessageBox.Ok)
            return ret

        self.update_config()
        self.close()

    def update_config(self):
        config["User"]["email"] = self.emailWidget.text()
        configurator.write(config)

class QTableWidgetDialog(QWidget):
    """
    This "window" is a QWidget. If it has no parent, it
    will appear as a free-floating window as we want.
    """
    def __init__(self, csvfilepath):
        QWidget.__init__(self)

        self.csvfilepath = csvfilepath

        self.table = QTableWidget()
        self.table_items = 0
        self.headerItems = []
        self.mailinglist_name = QLineEdit(Path(csvfilepath).stem)

        self.delimiter = QLineEdit(",")
        self.delimiter.textChanged.connect(self._read_csv)

        self.nom_column = QSpinBox()
        self.nom_column.label = "Nom"
        self.nom_column.valueChanged.connect(self.changeHorizontalHeader)
        self.prenom_column = QSpinBox()
        self.prenom_column.label = "Prénom"
        self.prenom_column.valueChanged.connect(self.changeHorizontalHeader)
        self.email_column = QSpinBox()
        self.email_column.label = "Email"
        self.email_column.valueChanged.connect(self.changeHorizontalHeader)
        self.spinboxes = [self.nom_column, self.prenom_column, self.email_column]
        self.spinboxes_values = {}
        self.remove_first_row_btn = QCheckBox("Supprimer la première ligne")
        self.remove_first_row_btn.toggled.connect(self._remove_first_row)

        self.btn = QPushButton("Importer")
        self.btn.clicked.connect(self._updateObservers)
        self.delimiter = QLineEdit(",")


        #########################
        # Layout
        #########################
        main_layout = QVBoxLayout()
        # --
        name_hbox = QHBoxLayout()
        name_hbox.addWidget(QLabel("Nom :"))
        name_hbox.addWidget(self.mailinglist_name)
        main_layout.addLayout(name_hbox)
        # --
        delimiter_layout = QHBoxLayout()
        delimiter_layout.addWidget(QLabel("Delimiter"))
        delimiter_layout.addWidget(self.delimiter)
        main_layout.addLayout(delimiter_layout)
        # -- Interagir avec l’entete de la table
        groupBox = QGroupBox("En-têtes")
        hbox = QHBoxLayout()
        hbox.addWidget(QLabel("Nom"))
        hbox.addWidget(self.nom_column)
        hbox.addWidget(QLabel("Prénom"))
        hbox.addWidget(self.prenom_column)
        hbox.addWidget(QLabel("Email"))
        hbox.addWidget(self.email_column)
        hbox.addWidget(self.remove_first_row_btn)
        groupBox.setLayout(hbox)
        main_layout.addWidget(groupBox)

        # --
        main_layout.addWidget(self.table)
        # --
        main_layout.addWidget(self.btn)
        # --
        self.setLayout(main_layout)

        self.table_isUpdated = False
        self.data = []
        self._observers = []

        self._read_csv()


    def _remove_first_row(self, isChecked):

        if isChecked:
            self.headerItems = [self.table.takeItem(0,col) for col in range(self.table.columnCount())]
            self.data = self.data[1:]
            self.table.removeRow(0)
        else:
            self.table.insertRow(0) # Empty row
            for idx, hItem in enumerate(self.headerItems):
                self.table.setItem(0,idx, hItem)
            self.data = [[hitem.text() for hitem in self.headerItems]] + self.data


    def save_classe(self, data, headers):
        global config
        csvfilepath = configurator._CONFIG_DIR / f'{self.mailinglist_name.text().replace(" ", "_")}.csv'

        # TODO mettre un Dialog pour poser la question
        classe_name = self.mailinglist_name.text()
        if classe_name in config["Mailing"]:
            print(f"The name '{classe_name}' is already used. Do you want to replace it or rename your class?")
            return False
        if csvfilepath.is_file():
            print(f"{csvfilepath} exists. Do you want to replace it or rename it?")
            return False

        readers.csv_write(csvfilepath, data, headers)
        config["Mailing"][classe_name] = csvfilepath.name
        configurator.write(config)
        config = configurator.read()

        return True

    def _updateObservers(self):

        Label_vs_ID = {value.label:key-1 for key, value in self.spinboxes_values.items()}

        # Clean and format data
        students_family_names = [dat[Label_vs_ID["Nom"]] for dat in self.data]
        # Sort the students then return the indexes not the names
        argsort_family_names = sorted(range(len(students_family_names)), key=students_family_names.__getitem__)
        _data = []
        for i in argsort_family_names:
            dat = self.data[i]
            _data.append([dat[Label_vs_ID["Nom"]].upper(), dat[Label_vs_ID["Prénom"]].lower().capitalize(), dat[Label_vs_ID["Email"]]])
        # _data = []
        # for dat in self.data:
        #     _data.append([dat[Label_vs_ID["Nom"]].upper(), dat[Label_vs_ID["Prénom"]].lower().capitalize(), dat[Label_vs_ID["Email"]]])

        classe_isSaved = self.save_classe(_data, ["Nom", "Prénom", "Email"])
        if classe_isSaved:
            classe_name = self.mailinglist_name.text()
            kwargs = {"data":_data.copy(),
                      "headers":["Nom", "Prénom", "Email", "classe"],
                      "classe_name":classe_name,
                      "isChecked": True
                     }
            for observer in self._observers:
                observer(**kwargs)

            self.clear_table()
            self.close()


    def fill(self, data):
        """data is a list of list
        """
        nb_rows = min(11, len(data))
        if nb_rows == 0:
            return None

        if nb_rows<=1:
            return None

        nb_cols = max([len(d) for d in data])
        self.table.setColumnCount(nb_cols)
        self.table.setHorizontalHeaderLabels([str(i+1) for i in range(nb_cols) ])

        # Place These QSpinBox after initializing the horizontal header
        self.spinboxes_values = {1: self.nom_column,
                                 2: self.prenom_column,
                                 3: self.email_column
                                }
        self.ignoreChangeHorizontalHeader = True
        self.nom_column.setRange(0, nb_cols)
        self.nom_column.setValue(1)
        self.prenom_column.setRange(0, nb_cols)
        self.prenom_column.setValue(2)
        self.email_column.setRange(0, nb_cols)
        self.email_column.setValue(3)
        self.ignoreChangeHorizontalHeader = False
        # Add the label on the first three headers
        for idx, spinbox in self.spinboxes_values.items():
            headerItem = self.table.horizontalHeaderItem(idx-1)
            headerItem.setText(f"[{idx}] {spinbox.label}")

        for idx in range(nb_rows):
            self.table.insertRow(self.table_items)
            for ielmnt, elmnt in enumerate(data[idx]):
                item = QTableWidgetItem(elmnt)
                # Read Only (keep all previous flags and remove ItemIsEditable)
                item.setFlags(item.flags() & ~QtCore.Qt.ItemIsEditable)
                self.table.setItem(self.table_items, ielmnt, item)
            self.table_items += 1

        # self.headerRow = self.table.row()
        # self.headers = headers
        # self.data = data[1:]


    def changeHorizontalHeader(self):#, column, text):
        spinbox = self.sender()
        column = spinbox.value()
        if column != 0 and not self.ignoreChangeHorizontalHeader:

            item = self.table.horizontalHeaderItem(column-1)
            item.setText(f"[{column}] {spinbox.label}")

            previous_value = None
            for val in list(self.spinboxes_values.keys()):
                spin = self.spinboxes_values[val]
                if spin.label == spinbox.label:
                    previous_value = val
                    break

            tmp_spinbox = self.spinboxes_values.pop(spinbox.value(), None)
            self.spinboxes_values[spinbox.value()] = spinbox
            if tmp_spinbox is None:
                # Aucune spinbox n’a cette valeur donc je dois supprimer l’ancien lien
                _ = self.spinboxes_values.pop(previous_value, None)
                # The column label must be changed to the column Id
                self.table.horizontalHeaderItem(previous_value-1).setText(str(previous_value))
            else:
                if previous_value is not None:
                    self.spinboxes_values[previous_value] = tmp_spinbox
                    tmp_spinbox.setValue(previous_value)


    def _read_csv(self):
        self.clear_table()
        # self.table_isupdated = False
        if len(self.delimiter.text()) == 0:
            return None

        self.data = readers.csv_reader(self.csvfilepath, delimiter=self.delimiter.text())
        self.headers = []
        self.fill(self.data)


    def clear_table(self):
        self.table.clear()
        self.table.setRowCount(0)
        self.table_items = 0
        # self.data = []


    def add_observer(self, observer):
        self._observers.append(observer)



class TerminalWidget(QTextEdit):
    def __init__(self):
        super().__init__()
        self.setReadOnly(True)

    def echo(self, text):
        self.insertPlainText(text+"\n")



class SplitAndSend(QMainWindow):
    def __init__(self, arguments=[]):
        QMainWindow.__init__(self)

        self.loginWidget = QLogin()
        self.pdfname = None
        self.pdf = None
        self.init_ui()

        self.createActions()
        self.createMenus()

        self.arguments = arguments if arguments else []
        if len(self.arguments)>0 and self.arguments[0] == "--debug":
            self._debug_()
            

    def _debug_(self):
        self.fill_table(data=[ f"TEST_{i},Cedricktout_{i},cedricktout+test{i}@gmail.com".split(",") for i in range(2)], classe_name="test")

        pdfname = "/mnt/Tron/Workspace/Judy/DOC-sujet.pdf"
        self.pdfname = Path(pdfname)
        self.pdf = readers.PDFReader(pdfname)
        self.nbPages_spinBox.setRange(1, self.pdf.numPages)
        self.pdf_label.setText(self.pdfname.name)
        self._auto_compute_nbPages_by_qcm_if_activated()
        if self.check_if_sendBtn_is_available():
            self.sendBtn.setEnabled(True)

        self.emailSubject.setText("Test")
        self.emailEditor.setText("Contenu\n texte")


    def createActions(self):

        self.openPdfAct = QtGui.QAction("Open PDF...", self,
                # shortcut=QtGui.QKeySequence.Open,
                statusTip="Open a pdf", triggered=self._importPDF)

        # self.openCSVAct = QtGui.QAction("Open CSV...", self,
        #         # shortcut=QtGui.QKeySequence.Open,
        #         statusTip="Open a CSV", triggered=self._read_csv)
        self.openCSVAct2 = QtGui.QAction("Open CSV", self,
                # shortcut=QtGui.QKeySequence.Open,
                statusTip="Open a CSV", triggered=self._open_csv)

        self.openLoginAct = QtGui.QAction("Login", self,
                statusTip="Paramètre de connection",
                triggered=self.loginWidget.signin
        )

        # self.saveAct = QtGui.QAction("&Save", self,
        #         shortcut=QtGui.QKeySequence.Save,
        #         statusTip="Save the document to disk", triggered=self.save)

        # self.printAct = QtGui.QAction("&Print...", self,
        #         shortcut=QtGui.QKeySequence.Print,
        #         statusTip="Print the document", triggered=self.print_)

        # self.exitAct = QtGui.QAction("E&xit", self, shortcut="Ctrl+Q",
        #         statusTip="Exit the application", triggered=self.close)


    def createMenus(self):
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenu.addAction(self.openPdfAct)
        # self.fileMenu.addAction(self.openCSVAct)
        self.fileMenu.addAction(self.openCSVAct2)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.openLoginAct)


    def init_ui(self):
        self.setMinimumSize(900, 600)

        # ----- Lister les classes déjà importées -----
        self.mailing_names_widget = QListWidget()
        self.mailing_names_widget.itemChanged.connect(self.show_hide_mailing_list) # Do not use itemClicked
        self.mailing_names_widget.setMaximumWidth(150)
        # Fill the widget
        self.mailing_list = configurator.get_mailing_list(config)
        for mailing in self.mailing_list.values():
            self.add_mailing_to_ListTable(mailing["name"])


        # --- PDF Group
        self.pdf_groupBox = QGroupBox("QCMs en 1 unique pdf")
        self.pdf_label = QLabel("...")
        self.pdf_importBtn = QPushButton("Importer")
        self.pdf_importBtn.clicked.connect(self._importPDF)
        self.pdf_dialog = QFileDialog()
        pdf_layout = QVBoxLayout()
        hbox = QHBoxLayout()
        hbox.addWidget(self.pdf_label)
        hbox.addWidget(self.pdf_importBtn)
        pdf_layout.addLayout(hbox)

        hbox = QHBoxLayout()
        hbox.addWidget(QLabel("Nombre de pages par étudiant"))
        self.nbPages_spinBox = QSpinBox()
        self.nbPages_spinBox.setDisabled(True)
        self.nbPages_auto = QCheckBox()
        self.nbPages_auto.setCheckState(QtCore.Qt.Checked)
        self.nbPages_auto.toggled.connect(self._toggle_qcm_nbPages)
        hbox.addWidget(self.nbPages_spinBox)
        hbox.addStretch(2)
        hbox.addWidget(self.nbPages_auto)
        hbox.addWidget(QLabel("Auto"))
        pdf_layout.addLayout(hbox)
        self.pdf_groupBox.setLayout(pdf_layout)

        # --- list of students
        self.tableWidget_nbRows = 0
        self.tableWidget = QTableWidget()
        self._init_tableWidget()
        # The column will stretch when the window is stretched
        self.tableWidget.horizontalHeader().setSectionResizeMode(2, QHeaderView.Stretch)
        self.csv_dialog = QFileDialog()
        

        # 
        # ------- Email Editor -------
        #
        _emailText = config["Email_editor"]["contents"]
        self.emailSubject = QLineEdit()
        self.emailEditor = QTextEdit()
        self.emailSubject.setPlaceholderText(config["Email_editor"]["subject"])
        self.emailEditor.setPlaceholderText(_emailText)

        self.sendBtn = QPushButton("Envoyer")
        self.sendBtn.setEnabled(False)
        self.sendBtn.clicked.connect(self._send)

        self.sendProgressBar = QProgressBar()
        self.sendProgressBar.hide()

        self.emailLayout = QVBoxLayout()
        self.emailLayout.addWidget(self.emailSubject)
        self.emailLayout.addWidget(self.emailEditor)
        self.emailLayout.addWidget(self.pdf_groupBox)
        self.emailLayout.addWidget(self.sendBtn)
        self.emailLayout.addWidget(self.sendProgressBar)
        # 
        # ------- Email Editor -------
        #


        self.terminal = TerminalWidget()
        self.terminal.hide()
        # self.terminal.resize()


        self.main_widget = QWidget()
        self.layout = QHBoxLayout()
        self.layout.addWidget(self.mailing_names_widget)
        splitter = QSplitter()
        splitter.addWidget(self.tableWidget)
        # Wrap the emailLayout in a Widget to add it in the splitter
        tempWidget = QWidget()
        tempWidget.setLayout(self.emailLayout)
        splitter.addWidget(tempWidget)
        # self.layout.addWidget(self.tableWidget)
        # self.layout.addLayout(self.emailLayout)
        self.layout.addWidget(splitter)

        main_splitter = QSplitter()
        main_splitter.setOrientation(QtCore.Qt.Vertical)
        mailinglist_table_email_Widget = QWidget()
        mailinglist_table_email_Widget.setLayout(self.layout)
        main_splitter.addWidget(mailinglist_table_email_Widget)
        main_splitter.addWidget(self.terminal)

        # self.main_widget.setLayout(self.layout)
        # self.setCentralWidget(self.main_widget)
        self.setCentralWidget(main_splitter)

    def check_if_sendBtn_is_available(self):
        return self.pdfname is not None and self.tableWidget.rowCount() != 0 and not self.sendBtn.isEnabled()

    def _toggle_qcm_nbPages(self, isChecked):
        self.nbPages_spinBox.setDisabled(isChecked)
        self._auto_compute_nbPages_by_qcm_if_activated()

    def _auto_compute_nbPages_by_qcm_if_activated(self):
        is_activated = self.nbPages_auto.isChecked()
        nb_receivers = self.tableWidget.rowCount()
        if is_activated and nb_receivers > 0 and self.pdf:
            assert self.pdf.numPages%nb_receivers==0, f"le nombre de Pages [{self.pdf.numPages}] n’est pas proportionnel au nombre de destinataires [{nb_receivers}]"
            self.nbPages_spinBox.setValue(self.pdf.numPages//nb_receivers)

    def _init_tableWidget(self):
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setHorizontalHeaderLabels(["Nom", "Prenom", "Email", "Classe"])

    # Will be an observer of the importation of CSV
    def add_mailing_to_ListTable(self, classe_name, isChecked=False, **kwargs):
        """ Add a QCheckbox to un/select the classe

        Parameters
        ----------
        classe : dict
            Its keys are : name, filepath
        
        The QCheckbox will be stored in classe["btn"].
        """

        item = QListWidgetItem()
        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
        if not isChecked:
            item.setCheckState(QtCore.Qt.Unchecked)
        else:
            item.setCheckState(QtCore.Qt.Checked)
        item.setText(classe_name)
        self.mailing_names_widget.addItem(item)

        return item

    def get_mailing_list(self):
        """Get the mailing lists"""
        return configurator.get_mailing_list(config)

    def show_hide_mailing_list(self, item):
        isChecked = True if item.checkState()==QtCore.Qt.Checked else False
        if isChecked:
            data = readers.csv_reader(self.mailing_list[item.text()]["filepath"])
            # data[0] contains the headers
            self.fill_table(data=data[1:], classe_name=item.text())
            self._auto_compute_nbPages_by_qcm_if_activated()
            if self.check_if_sendBtn_is_available():
                self.sendBtn.setEnabled(True)

        else:
            ### Methode 1 : chercher dans tout le tableau
            # itemsToRemove = self.tableWidget.findItems(item.text(), QtCore.Qt.MatchExactly)
            # for item in itemsToRemove:
            #     # QTableWidget.row(item) return the number of the row which contains the item
            #     self.tableWidget.removeRow(self.tableWidget.row(item))
            #     self.tableWidget_nbRows -= 1

            ### Methode 2 : chercher dans 1 seul colonne
            for idRow in range(self.tableWidget_nbRows)[::-1]:
                # Astuce: Commencer la recheche par le bas du tableau pour que la suppression
                # d’une ligne ne modifie pas le numero des autres lignes non vérifiées
                classe_item = self.tableWidget.item(idRow, CLASSE_COLUMN)
                if classe_item.text() == item.text():
                    self.tableWidget.removeRow(self.tableWidget.row(classe_item))
                    self.tableWidget_nbRows -= 1
            self._auto_compute_nbPages_by_qcm_if_activated()
            if not self.check_if_sendBtn_is_available():
                self.sendBtn.setEnabled(False)

    def fill_table(self, data=None, classe_name=None, **kwargs):

        for student in data:
            self.tableWidget.insertRow(self.tableWidget_nbRows)
            for ielmnt, elmnt in enumerate(student):
                item = QTableWidgetItem(elmnt)
                # Read Only (keep all previous flags and remove ItemIsEditable)
                item.setFlags(item.flags() & ~QtCore.Qt.ItemIsEditable)
                self.tableWidget.setItem(self.tableWidget_nbRows, ielmnt, item)
            if classe_name:
                item = QTableWidgetItem(classe_name)
                # Read Only (keep all previous flags and remove ItemIsEditable)
                item.setFlags(item.flags() & ~QtCore.Qt.ItemIsEditable)
                self.tableWidget.setItem(self.tableWidget_nbRows, ielmnt+1, item)
            self.tableWidget_nbRows += 1


    def get_students(self):
        # TODO headers Should be lowercase
        header = [self.tableWidget.horizontalHeaderItem(col).text() for col in range(self.tableWidget.columnCount())]
        
        students = []
        for row in range(self.tableWidget.rowCount()):
            students.append([])
            for col in range(self.tableWidget.columnCount()):
                item = self.tableWidget.item(row, col)
                if item:
                    students[-1].append(item.text())
                else:
                    students[-1].append("")

        return header, students

    def _importPDF(self):
        pdfname, _ = self.pdf_dialog.getOpenFileName(caption="Open File", filter="PDF (*.pdf)")
        if pdfname:
            self.pdfname = Path(pdfname)
            self.pdf = readers.PDFReader(pdfname)

            self.nbPages_spinBox.setRange(1, self.pdf.numPages)
            self.pdf_label.setText(self.pdfname.name)
            self._auto_compute_nbPages_by_qcm_if_activated()
            if self.check_if_sendBtn_is_available():
                self.sendBtn.setEnabled(True)

    def _include_name_in_pdfname(self, pdffilepath, nom, prenom):
        prefix = self.pdfname.stem
        new_name = f"{prefix}_{nom}_{prenom}.pdf"

        # pdffilepath.rename
        return pdffilepath.parent / new_name

    # @Slot()
    def _send(self):

        # TODO add a progress bar
        email_contents = self.emailEditor #.toPlainText()
        # email_contents = self.emailEditor.toHtml()
        email_contents_asTemplate = Template(email_contents.toHtml())
        # TODO renvoyé une erreur dû au template (variable inconnue...)
        # # ------- Step 1 Split pdf
        if not self.pdfname:
            print("A pdf is expected")
            return None
        print("Envoie en cours")

        nb_receivers = self.tableWidget.rowCount()
        if self.pdf.numPages != nb_receivers*self.nbPages_spinBox.value():
            print(f"le nombre de Pages [{self.pdf.numPages}] n’est pas proportionnel au nombre de destinataires [{nb_receivers*self.nbPages_spinBox.value()}]")
            return None

        self.terminal.show()
        _, mailing_list = self.get_students()
        # ------- Step 2: Create a QThread object
        self.thread = QtCore.QThread()
        # ------- Step 3: Create a worker object
        self.worker = Worker()
        self.worker.init_email(config["User"]["email"],
                               self.emailSubject.text(),
                               email_contents_asTemplate,
                               mailing_list
                              )
        self.worker.init_attachment(self.pdfname,
                                    self.nbPages_spinBox.value()
                                   )
        # ------- Step 4: move worker to the thread
        self.worker.moveToThread(self.thread)
        # ------- Step 5: connect signals and slots
        self.thread.started.connect(self.worker.run)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.worker.progress.connect(
            lambda i: self.sendProgressBar.setValue(i)
        )
        self.worker.logText.connect(
            lambda s: self.terminal.echo(s)
        )
        # ------- Step 6: Start the thread
        self.thread.start()


        # Final resets
        sendBtn_text = self.sendBtn.text()
        self.sendBtn.setDisabled(True)
        self.sendBtn.setText("Envoie en cours. Ceci peut prendre plusieurs minutes...")
        self.sendProgressBar.show()
        self.thread.finished.connect(
            lambda: self.sendBtn.setEnabled(True)
        )
        self.thread.finished.connect(
            lambda: self.sendBtn.setText(sendBtn_text)
        )
        # config["Email_editor"]["contents_MK"] = self.emailEditor.toMarkdown()
        # config["Email_editor"]["contents_HT"] = self.emailEditor.toHtml()
        # configurator.write(config)


    def _open_csv(self):
        # TODO mettre un bouton apply pour le changement de delimiter
        # Mettre un bouton OK, pour accepter le CSV
        # def show_new_window(self, checked):
        csvfilepath, _ = self.csv_dialog.getOpenFileName(caption="Open CSV", filter="CSV (*.csv)")

        if csvfilepath:
            self.w = QTableWidgetDialog(csvfilepath)#, self.students)
            self.w.add_observer(self.fill_table)
            self.w.add_observer(self.add_mailing_to_ListTable)
            self.w.show()




if __name__ == "__main__":
    if not Path(configurator._CONFIGFILE).is_file():
        configurator.initialize()

    config = configurator.read()
    configurator.check_files(config)
    config = configurator.read()
    app = QApplication(sys.argv)

    window = SplitAndSend(app.arguments()[1:])
    window.show()
    sys.exit(app.exec())
