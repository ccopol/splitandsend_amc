import json
with open("conf.json", encoding="utf-8") as fp:
    conf = json.load(fp)

csvfile = conf["csvfile"]
project_name = conf["project_name"]

from pathlib import Path

import readers

QCM = Path.home() / "Projets-QCM"
project_dir = QCM / project_name / "cr/corrections/pdf"

assert project_dir.is_dir(), "Dossier introuvable"

data = readers.csv_reader(csvfile)
header = data[0]


name_col = None
Id_col = None
email_col = None
for idx, head in enumerate(header):
    if head.strip().lower() == 'name':
        name_col = idx
        continue
    elif head.strip().lower() == 'id':
        Id_col = idx
        continue
    elif head.strip().lower() == 'E-mail 1 - Value'.lower():
        email_col = idx
        continue

assert Id_col is not None, "La colonne ID est manquante dans le csv"
assert name_col is not None, "La colonne Nom est manquante dans le csv"
assert email_col is not None, "La colonne Email est manquante dans le csv"




students = {}
for dat in data[1:]:
    assert dat[Id_col] not in students, f"Cet ID {dat[ID_col]} est déjà utilisé"
    students[ int(dat[Id_col]) ] ={
        'Name': dat[name_col],
        'ID': int(dat[Id_col]),
        'Email': dat[email_col]
    }
# assert "Prenom" in students, "La colonne Prénom est "

import yagmail


nb_emails = 0
no_pdf = []
for ID, student in students.items():
    pdfname = project_dir / f"{ID:04}-{student['Name']}.pdf"
    if not pdfname.is_file():
        no_pdf.append(ID)
        continue
    with yagmail.SMTP(conf["email"]) as yag:#, open(mail_envoye, "w") as csv_for_AMC:
        # print("Nom,Prenom,Email,PDF", file=csv_for_AMC)
        print(student['Name'],"...", end=" ")
        try:
            yag.send(student["Email"], conf["subject"], conf["content"], attachments=pdfname)
            #yag.send("c.n.copol@gmail.com", conf["subject"], conf["content"], attachments=pdfname)
            nb_emails += 1
        except KeyError as valerr:
            print(f"Email {student['Nom']} non envoyé {valerr}.")

        print("envoyé")
    # shutil.rmtree(pdftempdir)

    # self.logText.emit("Envoie terminée")
    # self.logText.emit(f"CSV sauvegardé: {self.pdfname.with_suffix('_for_AMC.csv')}")
    # self.finished.emit()

    #student[pdf] = pdfname


print(nb_emails, "envoyés et", len(no_pdf), "ABS")

# IDs_done = []
# for pdfname in project_dir.glob("*.pdf"):
#     stem = pdfname.stem
#     Id = int(stem[:4])
#     name = stem[5:]
#     student = students[Id]
#     assert name == student['Name'], f"*{name}* vs *{student['Name']}*, {pdfname.name}"

#     IDs_done.append(Id)


for ID in students:
    if ID in no_pdf:
        print("Cet étudiant a été ABS", students[ID]['Name'])
