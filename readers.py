import csv
from pathlib import Path
import tempfile

from PyPDF2 import PdfFileReader, PdfFileWriter


# ----------------------
#       CSV
# ----------------------

def csv_reader(csvfilepath, delimiter=","):
    if not Path(csvfilepath).is_file:
        raise FileNotFoundError("Can't find the CSV file")
    
    data = []
    with open(csvfilepath, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=delimiter)#, quotechar='|')
        for row in spamreader:
            data.append([r.strip() for r in row])

    return data


def csv_write(csvfilepath, data, headers, delimiter=","):
    with open(csvfilepath, 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(headers)
        for d in data:
            spamwriter.writerow(d)


# ----------------------
#       PDF
# ----------------------

def PDFReader(pdffile):
    return PdfFileReader(pdffile)


def PDFSplit(input_pdf, numPageByQCM):
    """
    Parameters
    ----------
    input_pdf: path like to a pdf
    numPageByQCM: int

    Returns
    -------
    pdflist: list of path list
    tempdir: tempory directory where the PDFs was saved
    """
    # TODO Convert to generator

    pdf = PdfFileReader(input_pdf)
    assert pdf.numPages%numPageByQCM == 0

    nbQcm = pdf.numPages // numPageByQCM
    start = 0
    end = numPageByQCM


    pdflist = []
    with tempfile.TemporaryDirectory(prefix="splitandsend") as tmpdirname:

        tempdir = tempfile.mkdtemp(prefix="splitandsend")
        for qcmIdx in range(nbQcm):
            pdfname = Path(input_pdf).stem + "_"
            with open(pdffile:=tempfile.mkstemp(prefix=pdfname, suffix=".pdf", dir=tempdir)[1], "wb") as output:
                pdf_writer = PdfFileWriter()
                for page in range(start, end):
                    pdf_writer.addPage(pdf.getPage(page))
                pdf_writer.write(output)                    

                pdflist.append(Path(pdffile))
                start = end
                end += numPageByQCM 
        
    return pdflist, tempdir